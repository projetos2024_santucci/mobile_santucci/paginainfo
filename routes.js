import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import detalheTarefas from './src/detalheDasTarefas';
import listaTarefas from './src/listaTarefas';

import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons'

const Tab = createBottomTabNavigator()

function Routes() {
  return (
    <Tab.Navigator
            screenOptions={{
                tabBarActiveTintColor: 'white',
                tabBarInactiveTintColor: '#f1f1f1',
                tabBarActiveBackgroundColor:'black',
                tabBarInactiveBackgroundColor:'black',
                
                tabBarShowLabel: false,
                tabBarStyle:{
                    position: 'absolute',
                    backgroundColor: 'white',
                    borderTopWidth: 0.15,
                }
            }}
        >
            <Tab.Screen
                name="Página Inicial"
                component={listaTarefas}
                options={{
                    headerShown: true,
                    tabBarIcon: ({color, size, focused}) =>{
                        if(focused){
                            return <Ionicons name="duplicate" size={size} color={color}/>
                        }

                        return <Ionicons name="duplicate-outline" size={size} color={color}/>
                    }
                }}
            />
            <Tab.Screen
                name="pagDetalheTarefas"
                component={detalheTarefas}
                options={{
                    headerShown: true,
                    tabBarIcon: ({color, size, focused}) =>{
                        if(focused){
                            return <Ionicons name="terminal" size={size} color={color}/>
                        }

                        return <Ionicons name="terminal-outline" size={size} color={color}/>
                    }
                }}
            />
          
                        
        </Tab.Navigator>
    );
}

export default Routes

import React from 'react';
import { FlatList, TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';

export default function detalheTarefas() {
  const tasks = [
    {
      id: 1,
      titulo: "Comprar leite",
      descricao: "Leite integral, 1 litro.",
      prioridade: "alta",
      status: "pendente",
    },
    {
        id: 2,
        titulo: "Fazer a lição de matemática",
        descricao: "Resolver os exercícios do capítulo 5.",
        prioridade: "media",
        status: "pendente",
      },
      {
        id: 3,
        titulo: "Marcar consulta médica",
        descricao: "Consulta de rotina com o Dr. Silva.",
        prioridade: "alta",
        status: "pendente",
      },
      {
        id: 4,
        titulo: "Levar o carro para o lava-jato",
        descricao: "Lavagem completa interna e externa.",
        prioridade: "baixa",
        status: "pendente",
      },
      {
        id: 5,
        titulo: "Responder e-mails",
        descricao: "Responder e-mails de trabalho e pessoais.",
        prioridade: "media",
        status: "pendente",
      },
      {
        id: 6,
        titulo: "Fazer compras no mercado",
        descricao: "Comprar frutas, legumes, verduras e outros alimentos.",
        prioridade: "alta",
        status: "pendente",
      },
      {
        id: 7,
        titulo: "Pagar as contas",
        descricao: "Pagar conta de luz, água, internet e telefone.",
        prioridade: "alta",
        status: "pendente",
      },
      {
        id: 8,
        titulo: "Organizar o armário",
        descricao: "Dobrar roupas e organizar as prateleiras.",
        prioridade: "baixa",
        status: "pendente",
      },
      {
        id: 9,
        titulo: "Ler um livro",
        descricao: "Ler o livro 'O Senhor dos Anéis'.",
        prioridade: "baixa",
        status: "pendente",
      },
      {
        id: 10,
        titulo: "Assistir um filme",
        descricao: "Assistir o filme 'O Poderoso Chefão'.",
        prioridade: "baixa",
        status: "pendente",
      },
      {
        id: 11,
        titulo: "Estudar para a prova",
        descricao: "Revisar os conteúdos para a prova de história.",
        prioridade: "alta",
        status: "pendente",
      },
      {
        id: 12,
        titulo: "Preparar o jantar",
        descricao: "Cozinhar uma refeição nutritiva e saborosa.",
        prioridade: "media",
        status: "pendente",
      },
      {
        id: 13,
        titulo: "Limpar a casa",
        descricao: "Arrumar e limpar os cômodos da casa.",
        prioridade: "baixa",
        status: "pendente",
      },
      {
        id: 14,
        titulo: "Fazer exercícios físicos",
        descricao: "Caminhada, corrida ou academia.",
        prioridade: "alta",
        status: "pendente",
      },
      {
        id: 15,
        titulo: "Meditar",
        descricao: "Praticar meditação por 10 minutos.",
        prioridade: "media",
        status: "pendente",
      },
      {
        id: 16,
        titulo: "Telefonar para a mãe",
        descricao: "Conversar com a mãe e saber como ela está.",
        prioridade: "alta",
        status: "pendente",
      },
      {
        id: 17,
        titulo: "Responder mensagens",
        descricao: "Responder mensagens de texto e WhatsApp.",
        prioridade: "media",
        status: "pendente",
      },
      {
        id: 18,
        titulo: "Atualizar o currículo",
        descricao: "Incluir novas experiências e habilidades.",
        prioridade: "baixa",
        status: "pendente",
      }
  ];

  const navigation = useNavigation();

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      marginTop: 30,
    },
    textUser: {
      fontSize: 24,
      fontWeight: 'bold',
      marginBottom: 5,
    },
    textContainer: {

      borderWidth: 0.4,
      margin: 5,
      borderRadius: 8,
      fontSize: 16,
      fontWeight: '500',
      padding: 5,
      color: 'black', 
      paddingStart: 15,
      paddingEnd: 10,
      
    },
   
  });

  const renderItem = ({ item }) => (
    <TouchableOpacity onPress={() => navigation.navigate('DetalheTarefa', { tarefa: item })}> 
      <View style={styles.list}>
        <Text style={styles.textContainer}>
            {item.titulo} - {item.descricao} - {item.prioridade} - {item.status}
        </Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      <FlatList
        data={tasks}
        keyExtractor={(item) => item.id.toString()}
        renderItem={renderItem}
      />
    </View>
  );
}

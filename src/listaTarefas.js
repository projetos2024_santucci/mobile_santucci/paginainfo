import React from 'react';
import { View, Button, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';

export default function listaTarefas() {
    const navegar = useNavigation()
    navegar.setOptions({
        headerTitle: 'Usuários',
        headerStyle: 
        { backgroundColor: 'black',},
        headerTintColor: '#fff',
      });

    const botaoTarefas = () => {
        navegar.navigate("pagDetalheTarefas")
    }
    
    

 return (
   <View>

    <Button title="Ver detalhe das tarefas" onPress={botaoTarefas}></Button>

    
   </View>
  );

}